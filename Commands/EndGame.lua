-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "End game",
		parameterDefs = {}
	}
end

-- @description return current wind statistics
function Run(self, units, parameter)
    message.SendRules({
        subject = "manualMissionEnd",
        data = {},
    })
	return SUCCESS
end

function Reset(self)
end