function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move a group of units along a path (not in any specified formation).",
		parameterDefs = {
			{ 
				name = "unitToMove",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "reverse",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 200

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetGroundHeight = Spring.GetGroundHeight

function Run(self, units, parameter)
	local unitToMove = parameter.unitToMove
	local path = parameter.path 
	local revert = parameter.reverse 

	local cmdID = CMD.MOVE
	
	if path == nil then
		return SUCCESS
	end
	
	if not self.started then
		if revert then
			for j=#path-1, 1, -1 do
				SpringGiveOrderToUnit(unitToMove, cmdID, path[j]:AsSpringVector(), {})
			end	
		else
			for j=2, #path do
				SpringGiveOrderToUnit(unitToMove, cmdID, path[j]:AsSpringVector(), {})
			end	
		end
		self.started = true
		self.threshold = THRESHOLD_DEFAULT
		return RUNNING
	end

	if SpringGetUnitHealth(unitToMove) == nil then
		return FAILURE
	end
	
	local x,y,z = SpringGetUnitPosition(unitToMove)
	local location = Vec3(x, SpringGetGroundHeight(x, z), z)
	local endPointIdx
	if revert then 
		endPointIdx = 1 
	else 
		endPointIdx = #path 
	end
	local distance = location:Distance(path[endPointIdx])
	-- Spring.Echo(distance)	
	if distance > self.threshold then
		return RUNNING
	end

	
	return SUCCESS
end

function Reset(self)
	self.threshold = THRESHOLD_DEFAULT
	self.started = false
end