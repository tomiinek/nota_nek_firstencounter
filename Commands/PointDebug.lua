-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "locations",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- @description return current wind statistics
function Run(self, units, parameter)
	
	for k, location in pairs(parameter.locations) do
		if (Script.LuaUI('exampleDebug_update')) then
			Script.LuaUI.exampleDebug_update(
				math.random(1,100), -- key
				{	-- data
					startPos = location, 
					endPos = location + Vec3(5, 0, 5)
				}
			)
		end
	end
	
	return SUCCESS
end

function Reset(self)
end