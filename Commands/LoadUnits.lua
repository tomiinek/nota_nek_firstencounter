function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Loads units into transporters.",
		parameterDefs = {
			{ 
				name = "unitsToLoad",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporters",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitCommands = Spring.GetUnitCommands
local SpringGetUnitHealth = Spring.GetUnitHealth

local function Contains(tbl, element)
	for _, value in pairs(tbl) do
		if value == element then
			return true
		end
	end
	return false
end

local function ClearState(self)
	self.capacity = 0
	self.loading = false
end

function Run(self, units, parameter)
	local unitsToLoad = parameter.unitsToLoad
	local transporters = parameter.transporters
	
	if (type(unitsToLoad) ~= "table") then
	  unitsToLoad = { unitsToLoad }
	end
	
	if (type(transporters) ~= "table") then
	  transporters = { transporters }
	end

	if not self.loading then
	
		local capacities = {}
		self.capacity = 0
		for i=1, #transporters do
			capacities[#capacities + 1] = 0
			local def = SpringGetUnitDefID(transporters[i])
			if def ~= nil then
				local transporterCapacity = UnitDefs[def].transportCapacity		
				self.capacity = self.capacity + transporterCapacity
			end
		end
		
		j = -1
		for i=1, #unitsToLoad do
			for k=1, #transporters do
				j = (j+1) % #transporters
				local def = SpringGetUnitDefID(transporters[j + 1])
				if def ~= nil then
					if capacities[j + 1] < UnitDefs[def].transportCapacity then
						capacities[j + 1] = capacities[j + 1] + 1
						SpringGiveOrderToUnit(transporters[j + 1], CMD.LOAD_UNITS, {unitsToLoad[i]}, {"shift"})
						break
					end
				else 
					break
				end
			end
		end
		
		self.loading = true
		return RUNNING
	end
	
	local allDead = true
	for i=1, #transporters do
		if SpringGetUnitHealth(transporters[i]) ~= nil then
			allDead = false
		end
	end
	
	if allDead then 
		return FAILURE
	end
	
	for i=1, #transporters do
		local loaded = SpringGetUnitIsTransporting(transporters[i])
        local commands = SpringGetUnitCommands(transporters[i])
		local health = SpringGetUnitHealth(transporters[i])
        if health ~= nil and loaded ~= nil and (commands ~= nil and #commands > 0) then
            return RUNNING
        end
	end
	
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end
