function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Moves a unit to a relative position.",
		parameterDefs = {
			{ 
				name = "unitToMove",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "relativeVector",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 128

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetGroundHeight = Spring.GetGroundHeight

function Run(self, units, parameter)
	local unitsToMove = parameter.unitToMove or units
	local relativeVector = parameter.relativeVector 
	
	if relativeVector == nil or unitsToMove == nil then
		return SUCCESS
	end
	
	if (type(unitsToMove) ~= "table") then
	  unitsToMove = { unitsToMove }
	end

	local cmdID = CMD.MOVE
	
	if not self.started then
		self.target = {}
		for i=1, #unitsToMove do
			local unitToMove = unitsToMove[i]
			local x,y,z = SpringGetUnitPosition(unitToMove)
			local location = Vec3(x, y, z)	
			local newX = x + relativeVector.x
			local newZ = z + relativeVector.z
			local targetLocation = Vec3(newX, SpringGetGroundHeight(newX, newZ), newZ)
			SpringGiveOrderToUnit(unitToMove, cmdID, targetLocation:AsSpringVector(), {})
			self.target[#self.target + 1] = targetLocation				
		end
		self.started = true
		self.threshold = THRESHOLD_DEFAULT	
		return RUNNING
	end
	
	for i=1, #unitsToMove do
		local x,y,z = SpringGetUnitPosition(unitsToMove[i])
		local location = Vec3(x, y, z)
		local distance = location:Distance(self.target[i])
		if SpringGetUnitHealth(unitsToMove[i]) ~= nil and distance > self.threshold then
			return RUNNING
		end
	end
	
	return SUCCESS
end

function Reset(self)
	self.threshold = THRESHOLD_DEFAULT
	self.started = false
	self.target = {}
end