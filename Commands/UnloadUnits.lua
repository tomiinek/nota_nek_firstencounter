function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Loads units into transporter.",
		parameterDefs = {
			{ 
				name = "location",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporters",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "safe",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitCommands = Spring.GetUnitCommands
local SpringGetUnitHealth = Spring.GetUnitHealth

function GetPosition(uid) 
	local x,y,z = SpringGetUnitPosition(uid)
	return Vec3(x,y,z)
end

local function Contains(tbl, element)
	for _, value in pairs(tbl) do
		if value == element then
			return true
		end
	end
	return false
end

local function ClearState(self)
	self.unloading = false
end

function Run(self, units, parameter)
	local location = parameter.location
	local transporters = parameter.transporters
	local safeUnload = parameter.safe
	
	if (type(transporters) ~= "table") then
	  transporters = { transporters }
	end
	
	if not self.unloading then
		self.unloading = true
		for i=1, #transporters do
			local loc = location or GetPosition(transporters[i])
			if safeUnload then
				SpringGiveOrderToUnit(transporters[i], CMD.UNLOAD_UNITS, {loc.x, loc.y, loc.z, 16}, {CMD.OPT_SHIFT})		
			else 
				SpringGiveOrderToUnit(transporters[i], CMD.UNLOAD_UNITS, {loc.x, loc.y, loc.z, 256}, {})
			end
		end
		return RUNNING
	end

	for i=1, #transporters do
		local loaded = SpringGetUnitIsTransporting(transporters[i])
        local commands = SpringGetUnitCommands(transporters[i])
		local health = SpringGetUnitHealth(transporters[i])
        if health ~= nil and ((loaded ~= nil and #loaded > 0) or (commands ~= nil and #commands > 0)) then
            return RUNNING
        end
	end
		
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end
