function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Moves a unit to a position.",
		parameterDefs = {
			{ 
				name = "unitToMove",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "location",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 156

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetGroundHeight = Spring.GetGroundHeight

function Run(self, units, parameter)
	local unitToMove = parameter.unitToMove or units
	local targetLocation = parameter.location 

	local cmdID = CMD.MOVE
	
	if targetLocation == nil or unitToMove == nil then
		return SUCCESS
	end
	
	if SpringGetUnitHealth(unitToMove) == nil then
		return FAILURE
	end
	
	local x,y,z = SpringGetUnitPosition(unitToMove)
	local location = Vec3(x, y, z)

	if not self.started then		
		SpringGiveOrderToUnit(unitToMove, cmdID, targetLocation:AsSpringVector(), {})
		self.started = true
		self.threshold = THRESHOLD_DEFAULT
		return RUNNING
	end

	local distance = location:Distance(targetLocation)
	-- Spring.Echo(distance)	
	if distance > self.threshold then
		return RUNNING
	end
	
	return SUCCESS
end

function Reset(self)
	self.threshold = THRESHOLD_DEFAULT
	self.started = false
end