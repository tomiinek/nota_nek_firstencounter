local sensorInfo = {
	name = "Units locations.",
	desc = "Returns location of given units or all units.",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns locations of given uids or all units if not specified
return function(uids)

	local unitIds = uids or units
	local locations = {}
	
	for i=1, #unitIds do 
		local uid = unitIds[i]
		local x,y,z = SpringGetUnitPosition(uid)
		locations[#locations + 1] = Vec3(x,y,z)
	end

	return locations
end