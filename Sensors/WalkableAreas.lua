local sensorInfo = {
	name = "Walkable areas",
	desc = "Creates walkable map with given resolution w.r.t. given enemies.",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight


function GetPosition(uid) 
	local x,y,z = SpringGetUnitPosition(uid)
	return Vec3(x,y,z)
end

-- @description return map of safe locations
return function(enemies, resolution, heightOffset)

	-- ugly constants based on emeny ranges etc.
	local MIN_GROUND_DIST = 4 * 256
	local MIN_HEIGHT_DIST = 640

	resolution = resolution or 64
	heightOffset = heightOffset or 0

	local tileSize = { x=(Game.mapSizeX / resolution), z=(Game.mapSizeZ / resolution)}
	local tileIdx = { x=(MIN_GROUND_DIST / tileSize.x), z=(MIN_GROUND_DIST / tileSize.z) }
	
	local enemyLocs = {}
	local map = {}
	map.tiles = {}
	map.size = resolution 
	map.tileSize = { x=tileSize.x, z=tileSize.z }

	for i=1, #enemies do
        enemyLocs[#enemyLocs + 1] = GetPosition(enemies[i])
    end
	
    for x=0, resolution do
		map.tiles[x + 1] = {}
		for z=0, resolution do
			local trueX = x * tileSize.x
			local trueZ = z * tileSize.x
            local groundLocation = Vec3(trueX, SpringGetGroundHeight(trueX, trueZ), trueZ) 
			map.tiles[x + 1][#map.tiles[x + 1] + 1] = { isSafe=true, location=groundLocation }
        end
    end
	
	for i=1, #enemyLocs do
		local enemy = enemyLocs[i]	

		enemyTileIdx = { x=math.floor(enemy.x / tileSize.x + 0.5), z=math.floor(enemy.z / tileSize.z + 0.5) }
		for x=math.max(0, enemyTileIdx.x - tileIdx.x), math.min(resolution, enemyTileIdx.x + tileIdx.x) do
			for z=math.max(0, enemyTileIdx.z - tileIdx.z), math.min(resolution, enemyTileIdx.z + tileIdx.z) do
				
				local tile = map.tiles[x+1][z+1]
				
				if tile.isSafe then
					local trueLocation = tile.location + Vec3(0, heightOffset, 0)
					local horizontalDist = enemy:Distance(trueLocation)
					
					if horizontalDist < MIN_GROUND_DIST then
						local verticalDist = math.abs(enemy.y - trueLocation.y)
						if MIN_HEIGHT_DIST / MIN_GROUND_DIST * (MIN_GROUND_DIST - horizontalDist) > verticalDist then
							map.tiles[x+1][z+1] = { isSafe=false, location=tile.location }
						end
					end
				end
				
			end -- y tiles iter
		end -- x tiles iter
		
    end -- enemy iter
	
    return map
end