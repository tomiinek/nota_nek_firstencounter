local sensorInfo = {
	name = "Units to map of uids.",
	desc = "Returns map of uids made out of units.",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description returns map of uids of units
return function()

	local uids = {}
	
	for i=1, units.length do 
		uids[i] = units[i]
	end

	return uids
end