local sensorInfo = {
	name = "Units except those in a circle.",
	desc = "Returns units which are not in the given circle (center, radius map).",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetPosition(uid) 
	local x,y,z = SpringGetUnitPosition(uid)
	return Vec3(x,y,z)
end

-- @description splits given list of units
return function(ids, circle, order)
    uids = ids or units
	orderByDist = order or false
	
	local outerUnits = {}

	for i=1, uids.length do
		local uid = uids[i]
		local distance = circle.center:Distance(GetPosition(uid))
		if distance > circle.radius then
			outerUnits[#outerUnits + 1] = { id=uid, d=distance}			
		end
	end
	
	if orderByDist then
		table.sort(outerUnits, function(a, b) return a.d < b.d end)
	end
	
	returnValue = {}	
	for i=1, #outerUnits do
		returnValue[#returnValue + 1] = outerUnits[i].id 
	end

	return returnValue
end