local sensorInfo = {
	name = "FindHills",
	desc = "Finds points which specify hills (nearby points are discarted).",
	author = "Tomiinek",
	date = "2019-04-22",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return hill points
return function(resolution, heightThreshold, groupingThreshold)

	resolution = resolution or 128
	heightThreshold = heightThreshold or 3/4
	groupingThreshold = groupingThreshold or 555
	
	local lowest, highest = Spring.GetGroundExtremes()	
	local hillPoints = {}

	for x=0, Game.mapSizeX, Game.mapSizeX / resolution do
		for z=0, Game.mapSizeZ, Game.mapSizeZ / resolution do
			local height = Spring.GetGroundOrigHeight(x, z)
			if height > lowest + (highest - lowest) * heightThreshold then
				hillPoints[#hillPoints + 1] = Vec3(x, height, z)
			end
		end
	end

	local hillCentroids = {}
	
	for k, newLocation in pairs(hillPoints) do

		local used = false
		for idx, centroid in pairs(hillCentroids) do
			if newLocation:Distance(centroid[1]) < groupingThreshold then
				local centroidSize = centroid[2]
				hillCentroids[idx][1] = (centroid[1] * centroidSize + newLocation) / (centroidSize + 1) 
				hillCentroids[idx][2] = centroidSize + 1
				used = true
			end
		end 

		if used == false then
			hillCentroids[#hillCentroids + 1] = {newLocation, 1}
		end
		
	end	
	
	local hills = {}
	
	for i, c in pairs(hillCentroids) do
		hills[i] = hillCentroids[i][1]
	end 
	
	return hills
end
