local sensorInfo = {
	name = "Pointwise path finding.",
	desc = "Returns map of some unit ids and their paths to goals or nil if no path exists for all units",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition
local heap = require"binary_heap"

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetPosition(uid) 
	local x,y,z = SpringGetUnitPosition(uid)
	return Vec3(x,y,z)
end

function Contains(tbl, element)
	for _, value in pairs(tbl) do
		if value == element then
			return true
		end
	end
	return false
end

function FindPath(from, to, map)

	function Heuristic(from, to)
		return math.sqrt((from.x - to.x) * (from.x - to.x) + (from.z - to.z) * (from.z - to.z))
	end
	
	local path = { from }

    local f = { x=math.min(map.size, math.max(0, math.floor(from.x / tileSize.x + 0.5))), z=math.min(map.size, math.max(0, math.floor(from.z / tileSize.z + 0.5)) }
	local goal = { x=math.min(map.size, math.max(0, math.floor(to.x / tileSize.x + 0.5))),   z=math.min(map.size, math.max(0, math.floor(to.z / tileSize.z + 0.5)) }
	local neighbours = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}}
	
	local closed = {}
	local open = heap:new()
	local cameFrom = {}
	local gScore, fScore = {}, {}
	
	gScore[f.x][f.z] = 0
	fScore[f.x][f.z] = gScore[f.x][f.x] + Heuristic(f, goal)
	open:insert(fScore[f.x][f.z], f)
	
	while ~open:empty() do
	
		local fScore, current = open:pop()
		
		if current.x == goal.x and current.y == goal.y then
			-- TODO:
			local path = unwind_path ( {}, came_from, goal )
			
			table.insert(path, goal)
			return path
		end
	
		table.insert(closed, current)
		
		for i=1, #neighbours do 
			local neighbour = { x=(current.x + neighbours[i][1]), y=(current.y + neighbours[i][2]) }
			if neighbour.x >= 0 and neighbour.z >= 0 and neighbour.x <= map.size and neighbour.z <= map.size then		
				if ~Contains(closed, neighbour) then
			
					-- TODO: 
					local tentative_g_score = g_score [ current ] + dist_between ( current, neighbor )
					 
					if not_in ( openset, neighbor ) or tentative_g_score < g_score [ neighbor ] then 
						came_from 	[ neighbor ] = current
						g_score 	[ neighbor ] = tentative_g_score
						f_score 	[ neighbor ] = g_score [ neighbor ] + heuristic_cost_estimate ( neighbor, goal )
						if not_in ( openset, neighbor ) then
							table.insert ( openset, neighbor )
						end
					end
					
				end
			end
		end
	end
	
	return nil -- no valid path
	
	
	
	

    local function neighborExistsSafeNotExploredOrBetter(nb, currLoc, navGraph, graphGrid, gridSizeX, gridSizeY)
        if not (nb.x >= 1 and nb.x <= gridSizeX) then return false end         -- neighbor not on map in x -> false  
        if not (nb.y >= 1 and nb.y <= gridSizeY) then return false end    -- neighbor not on map in y -> false  
        
        if not (graphGrid[nb.x][nb.y].safe) then return false end -- nb not safe -> false
        if navGraph[nb.x][nb.y] == nil then return true end       -- nb not visited -> true
        
        if navGraph[nb.x][nb.y].dis <= navGraph[currLoc.x][currLoc.y].dis then return false end -- if nb isn't from this front -> false  
        return graphGrid[currLoc.x][currLoc.y].loc.y < navGraph[nb.x][nb.y].predHig -- nb's prede is of same distance as curr and curr is better 
    end
    
    local nbs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}}
    local bfsQ = newQueue()

    local navGraph = {}
    navGraph[dest.x] = {}
    navGraph[dest.x][dest.y] = {pred = dest, predHig=graphGrid[dest.x][dest.y].loc.y, dis = 0}

    -- create predecessor based navGraph trough modified BFS
    bfsQ:push_right(dest)
    while not bfsQ:is_empty() do
        local currLoc = bfsQ:pop_right()
        for i=1, #nbs do
            local nb = {x = (currLoc.x + nbs[i][1]), y = (currLoc.y + nbs[i][2])}
            if navGraph[nb.x] == nil then navGraph[nb.x] = {} end   

            -- if the neighbor is safe & hasn't been explored yet -> add to navGraph & queue
            if neighborExistsSafeNotExploredOrBetter(nb, currLoc, navGraph, graphGrid, gridSizeX, gridSizeY) then
                if navGraph[nb.x][nb.y] == nil then bfsQ:push_left(nb) end -- need to check -> can be just updating pred with a lower hight 
                navGraph[nb.x][nb.y] = {pred= currLoc, predHig=graphGrid[currLoc.x][currLoc.y].loc.y, dis = navGraph[currLoc.x][currLoc.y].dis + 1}
            end

        end
    end


    -- find path in the navgraph trough going from start via .pred references
    if navGraph[start.x] == nil or navGraph[start.x][start.y] == nil then
        return {suc=false, dta=nil}
    end

    local currLoc = start
    local path = {graphGrid[currLoc.x][currLoc.y].loc}
    while currLoc.x ~= dest.x or currLoc.y ~= dest.y do
        currLoc = navGraph[currLoc.x][currLoc.y].pred
        path[#path+1] = graphGrid[currLoc.x][currLoc.y].loc
    end

	return {suc=true, dta=path}

	return nil
end

-- @description finds path for all units
return function(uids, goals, map)
    
	local paths = {}
	local reachedGoals = {}
	
	for i=1, uids.length do
		local uid = uids[i]
		local loc = GetPosition(uid)		
		for j=1, #goals do
			if ~Contains(reachedGoals, j) then
				local goal = goals[j]
				local path = FindPath(loc, to, map)
				if path ~= nil then
					reachedGoals[#reachedGoals + 1] = j
					paths[#paths + 1] = { uid=uid, path=path }
					break
				end
			end
		end
	end
	
	if #paths == 0 then
		return nil
	end
	
	return paths
end