local sensorInfo = {
	name = "Random location.",
	desc = "Returns a random location in a square given its side and center radius.",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description returns locations of given uids or all units if not specified
return function(center, radius)
	return Vec3(center.x + (math.random() - 0.5) * radius, center.y, center.z + (math.random() - 0.5) * radius)
end