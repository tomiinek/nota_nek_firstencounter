local sensorInfo = {
	name = "Path finding.",
	desc = "Finds path from uid to goal with contraints given as map.",
	author = "Tomiinek",
	date = "2019-05-04",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetPosition(uid) 
	local x,y,z = SpringGetUnitPosition(uid)
	return Vec3(x,y,z)
end

function Contains(tbl, element)
	for _, value in pairs(tbl) do
		if value == element then
			return true
		end
	end
	return false
end

-- @description finds path for all units
return function(uid, goal, map)

	local locGoal = GetPosition(goal)
	local loc = GetPosition(uid)
	
	return { loc, locGoal }
end