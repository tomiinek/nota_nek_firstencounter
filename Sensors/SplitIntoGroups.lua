local sensorInfo = {
	name = "Split units into groups.",
	desc = "Splits given list of units into a specified number of folds.",
	author = "Tomiinek",
	date = "2019-04-22",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description splits given list of units ids into n folds
return function(n, ids)
    uids = ids or units
	
    local folds = {}
	for i=1, n do
		folds[i] = {}
	end
	
    for i=1, #uids do
        local idx = (i - 1) % n  + 1
        folds[idx][#folds[idx] + 1] = uids[i]
    end

	return folds
end